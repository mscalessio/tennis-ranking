<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://alessiomasucci.com
 * @since             1.0.0
 * @package           Tennis_Ranking
 *
 * @wordpress-plugin
 * Plugin Name:       Tennis.it Ranking
 * Plugin URI:        http://www.tennis.it
 * Description:       Get the latest Top 100 rankings from ATP and WTA.
 * Version:           1.0.1
 * Author:            Alessio Masucci
 * Author URI:        https://alessiomasucci.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tennis-ranking
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tennis-ranking-activator.php
 */
function activate_tennis_ranking() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tennis-ranking-activator.php';
	Tennis_Ranking_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tennis-ranking-deactivator.php
 */
function deactivate_tennis_ranking() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tennis-ranking-deactivator.php';
	Tennis_Ranking_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tennis_ranking' );
register_deactivation_hook( __FILE__, 'deactivate_tennis_ranking' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tennis-ranking.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tennis_ranking() {

	$plugin = new Tennis_Ranking();
	$plugin->run();

}
run_tennis_ranking();
