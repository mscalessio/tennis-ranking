<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://alessiomasucci.com
 * @since      1.0.0
 *
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/public
 * @author     Alessio Masucci <msc.alessio@gmail.com>
 */
class Tennis_Ranking_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->ranking_options = [
			'apifier_user_id' 	=> 'wdtazXYbDx8EuBYaa',
			'apifier_token' 	=> 'bLydcoBWDyH6baJMdbw25Duxt',
		];

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tennis_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tennis_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tennis-ranking-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tennis_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tennis_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tennis-ranking-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add ranking ATP double shortcode.
	 *
	 * @since    1.0.0
	 */
	public function ranking_generate_atp_table( $crawler = null, $token = null ) {
		$api_url = 'https://api.apifier.com/v1/';
		$api_url .= $this->ranking_options['apifier_user_id'];
		$api_url .= '/crawlers/' . $crawler;
		$api_url .= '/lastExec/results?token=' . $token;
		$api_url .= '&format=json&simplified=1';
		$api_response = wp_remote_get($api_url);
		
		$json = wp_remote_retrieve_body( $api_response );

		if( empty( $json ) )
			return false;
		
		$json = json_decode( $json );
		$table = '<table class="ranking-atp-table"><thead><tr><th>Ranking</th><th>Move</th><th>Country</th><th>Player</th><th>Age</th><th>Points</th><th>Turn Played</th><th>Points Dropping</th><th>Next Best</th></tr></thead>';
		$table .= '<tbody>';
		foreach ($json as $key => $value) {
			$table .= $this->generate_atp_row($key, $value);
		}
		$table .= '</tbody></table>';
		return $table;
	}

	/**
	 * Add ranking ATP shortcode with parameters.
	 *
	 * @since    1.0.0
	 */
	public function ranking_atp_shortcode( $atts ) {

		// Attributes
		$atts = shortcode_atts(
			array(
				'type' => 'singles',
				'country' => 'all',
			),
			$atts
		);

		$crawler = 'ATP_Ranking_Single';
		
		if ($atts['country'] == 'ita') {
			if (strtolower($atts['type']) == 'singles') {
				$crawler = 'ATP_Ranking_All_Italian_Singles';
			} elseif (strtolower($atts['type']) == 'doubles') {
				$crawler = 'ATP_Ranking_All_Italian_Doubles';
			}
		} else {
			if (strtolower($atts['type']) == 'singles') {
				$crawler = 'ATP_Ranking_Single';
			} elseif (strtolower($atts['type']) == 'doubles') {
				$crawler = 'ATP_Ranking_Double';
			}
		}

		$table = $this->ranking_generate_atp_table($crawler, $this->ranking_options['apifier_token']);

		return $table;

	}

	/**
	 * Add ranking WTA shortcode with parameters.
	 *
	 * @since    1.0.0
	 */
	public function ranking_wta_shortcode($atts) {
		
		// Attributes
		$atts = shortcode_atts(
			array(
				'type' => 'singles',
				'country' => 'all',
			),
			$atts
		);

		$type = $atts['type'];

		$api_url = 'http://www.wtatennis.com/node/232054/'.$type.'/ranking.json';
		$api_response = wp_remote_get($api_url);
		
		$json = wp_remote_retrieve_body( $api_response );
		if( empty( $json ) )
			return false;
		
		$json = json_decode( $json );
		
		if ($atts['type'] == 'doubles')
			usort($json, array($this, "cmp"));

		if ($atts['country'] != 'ita')
			$json = array_slice($json, 0, 100);

		$table = '<table class="ranking-wta-table"><thead><tr><th>Ranking</th><th>Move</th><th>Country</th><th>Player</th><th>Age</th><th>Points</th><th>Turn Played</th></tr></thead><tbody>';
		foreach ($json as $key => $value) {
			$value->move = strip_tags($value->move);
			$value->fullname =  strip_tags(preg_replace('/<a[^>]*>([\s\S]*?)<\/a[^>]*>/', '', $value->fullname));
			$value->country = str_replace(array( '[', ']' ), '', strip_tags($value->country));
			
			if ($atts['country'] == 'ita') {
				if (strtolower($value->country) == 'ita') {
					$table .= $this->generate_wta_row($key, $value);
				}
			} else {
				$table .= $this->generate_wta_row($key, $value);
			}

		}
		$table .= '</tbody></table>';
		return $table;
	}

	/**
	 * Registers all shortcodes at once
	 *
	 * @return [type] [description]
	 */
	public function register_shortcodes() {
		add_shortcode( 'atp-ranking', array( $this, 'ranking_atp_shortcode' ) );
		add_shortcode( 'wta-ranking', array( $this, 'ranking_wta_shortcode' ) );
	}

	/**
	 * Generate a row with the data for the ATP table
	 *
	 * @since     1.0.0
	 * @return    string    The row of the table.
	 */
	private function generate_atp_row($key, $item) {
		$row = '<tr id="' . $key . '">';
		$row .= '<td class="rank-cell">' . $item->rank . '</td>';
		$row .= '<td class="move-cell">' . $item->move . '</td>';
		$row .= '<td class="country-cell"><img src="' . plugin_dir_url( __FILE__ ) . 'img/flags/' . strtolower($item->country) . '.svg" alt="' . $item->country . '"></td>';
		$row .= '<td class="player-cell">' . $item->player . '</td>';
		$row .= '<td class="age-cell">' . $item->age . '</td>';
		$row .= '<td class="points-cell">' . number_format($item->points, 0, '', '.') . '</td>';
		$row .= '<td class="tourn-cell">' . $item->tournPlayed . '</td>';
		$row .= '<td class="pts-cell">' . $item->pointsDropping . '</td>';
		$row .= '<td class="next-cell">' . $item->nextBest . '</td>';
		$row .= '</tr>';
		return $row;
	}

	/**
	 * Generate a row with the data for the WTA table
	 *
	 * @since     1.0.0
	 * @return    string    The row of the table.
	 */
	private function generate_wta_row($key, $item) {
		$row = '<tr id="' . $key . '">';
		$row .= '<td class="rank-cell">' . $item->rank . '</td>';
		$row .= '<td class="move-cell">' . $item->move . '</td>';
		$row .= '<td class="country-cell"><img src="' . plugin_dir_url( __FILE__ ) . 'img/flags/' . strtolower($item->country) . '.svg" alt="' . $item->country . '"></td>';
		$row .= '<td class="player-cell">' . $item->fullname . '</td>';
		$row .= '<td class="age-cell">' . $item->age . '</td>';
		$row .= '<td class="points-cell">' . number_format($item->points, 0, '', '.') . '</td>';
		$row .= '<td class="tourn-cell">' . $item->tourn . '</td>';
		$row .= '</tr>';
		return $row;
	}

	/**
	 * Function to order the objects by ranking.
	 *
	 * @since     1.0.0
	 */
	private function cmp($a, $b) {
		return $a->rank > $b->rank;
	}

}
