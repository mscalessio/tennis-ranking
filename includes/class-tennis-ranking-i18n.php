<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://alessiomasucci.com
 * @since      1.0.0
 *
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/includes
 * @author     Alessio Masucci <msc.alessio@gmail.com>
 */
class Tennis_Ranking_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'tennis-ranking',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
