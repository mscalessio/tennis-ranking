<?php

/**
 * Fired during plugin activation
 *
 * @link       https://alessiomasucci.com
 * @since      1.0.0
 *
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tennis_Ranking
 * @subpackage Tennis_Ranking/includes
 * @author     Alessio Masucci <msc.alessio@gmail.com>
 */
class Tennis_Ranking_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
