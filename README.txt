=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://alessiomasucci.com
Tags: tennis, ranking
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get the latest tennis rankings from the official ATP and WTA channels.

== Description ==

This plugin gives you the ability to get the latest Tennis rankings from the official ATP and WTA channels.

Funzionalità principali di Tennis Rankings:

* ATP Top 100 Singles Male
* ATP Top 100 Doubles Male
* ATP All Singles Italian Male
* ATP All Doubles Italian Male
* WTA Top 100 Singles Female
* WTA Top 100 Doubles Female
* WTA All Singles Italian Female
* WTA All Doubles Italian Female

    Note that the `readme.txt` of the stable tag is the one that is considered the defining one for the plugin, so
if the `/trunk/readme.txt` file says that the stable tag is `4.3`, then it is `/tags/4.3/readme.txt` that'll be used
for displaying information about the plugin.  In this situation, the only thing considered from the trunk `readme.txt`
is the stable tag pointer.  Thus, if you develop in trunk, you can update the trunk `readme.txt` to reflect changes in
your in-development version, without having that information incorrectly disclosed about the current stable version
that lacks those changes -- as long as the trunk's `readme.txt` points to the correct stable tag.

    If no stable tag is provided, it is assumed that trunk is stable, but you should specify "trunk" if that's where
you put the stable version, in order to eliminate any doubt.

== Installation ==

1. Upload `tennis-ranking` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place the shortcode `[atp-ranking]` or `[wta-ranking]` in your pages or posts

Here's the shortcode options available:

=== ATP ===

The default attributes is `[atp-ranking type="singles" country="all"]` so you can omit the attributes and get the same results.

The values allowed for `type` are: `type="singles"` or `type="doubles"`.
The values allowed for `country` are: `type="all"` or `type="ita"`.

So you can use:

* ATP Top 100 Singles Male      - `[atp-ranking type="singles" country="all"]`
* ATP Top 100 Doubles Male      - `[atp-ranking type="doubles" country="all"]`
* ATP All Singles Italian Male  - `[atp-ranking type="singles" country="ita"]`
* ATP All Doubles Italian Male  - `[atp-ranking type="doubles" country="ita"]`

=== WTA ===

The default attributes is `[wta-ranking type="singles" country="all"]` so you can omit the attributes and get the same results.

The values allowed for `type` are: `type="singles"` or `type="doubles"`.
The values allowed for `country` are: `type="all"` or `type="ita"`.

So you can use:

* WTA Top 100 Singles Female        - `[wta-ranking type="singles" country="all"]`
* WTA Top 100 Doubles Female        - `[wta-ranking type="doubles" country="all"]`
* WTA All Singles Italian Female    - `[wta-ranking type="singles" country="ita"]`
* WTA All Doubles Italian Female    - `[wta-ranking type="doubles" country="ita"]`

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* First release of this plugin.

== Upgrade Notice ==

= 1.0 =
First release of this plugin.